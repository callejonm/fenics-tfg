module mod_malla
!Modulo que contiene:
! La definición del tipo malla
! La subrutina de lectura de la malla
implicit none

!--------Tipo Malla------------------------------------------------------
! ne		numero de elementos
! n			numero de nodos
! nver		numero de vertices
! npe		nodos por elemento
! cpe		caras por elemento
! vpe		vertices por elemento
! ape		aristas por elemento
! mm         numeracion global de los vertices de cada elemento
! nn        numeracion global de los nodos de cada elemento	
!-----------------------------------------------------------------------
TYPE malla
	INTEGER::ne
	INTEGER::n
	INTEGER::nver
	INTEGER,ALLOCATABLE,dimension(:,:)::mm
	INTEGER,ALLOCATABLE,dimension(:,:)::nn
	INTEGER,ALLOCATABLE,dimension(:,:)::nra
	INTEGER,ALLOCATABLE,dimension(:,:)::nrv
        REAL*8,ALLOCATABLE,dimension(:,:)::z
        REAL*8,ALLOCATABLE,dimension(:,:)::znod
END TYPE malla

TYPE matxele
    REAL*8 :: mat(2,2) 
END TYPE matxele

TYPE vecxele
    REAL*8 :: ver(3) 
END TYPE vecxele

TYPE derxele
    REAL*8::mat(2,3)
END TYPE derxele


!La malla sera 2D y P1
INTEGER::npe=6,vpe=3,ape=3

contains

!Subrutina que lee la malla
SUBROUTINE leemallap2(mallap,nmalla,iformat)
TYPE(malla)::mallap
character(20)::nmalla
INTEGER::i,j,k,ne,nver,iformat,kk,n
!


if (iformat==0)then
        open(10,file=nmalla,form='unformatted',status='unknown')
        rewind(10)
        read(10) mallap%ne,mallap%n,mallap%nver
print*,'elementos', mallap%ne
print*,'nodos',mallap%n
print*,'vertices',mallap%nver
        ne=mallap%ne
        nver=mallap%nver
        n=mallap%n
        !dimensionamos
        allocate(mallap%nn(npe,ne), mallap%mm(vpe,ne), mallap%z(3,nver),&
                 mallap%nra(ape,ne), mallap%nrv(vpe,ne))

        read(10) ((mallap%nn(j,k),j=1,npe),k=1,mallap%ne) ,&
                 ((mallap%mm(j,k),j=1,vpe),k=1,mallap%ne) ,&
                 ((mallap%nra(j,k),j=1,ape),k=1,mallap%ne),&
                 ((mallap%nrv(j,k),j=1,vpe),k=1,mallap%ne),&
                  ((mallap%z(j,i),j=1,2),i=1,mallap%nver)
else
        open(10,file=nmalla,form='formatted',status='unknown')
        rewind(10)
        read(10,*) mallap%ne,mallap%n,mallap%nver
        ne=mallap%ne
        nver=mallap%nver
        n=mallap%n
        !dimensionamos
        allocate(mallap%nn(npe,ne),mallap%mm(vpe,ne),mallap%z(3,nver),&
                mallap%nra(ape,ne),mallap%nrv(vpe,ne))

        read(10,*) ((mallap%nn(j,k),j=1,npe),k=1,mallap%ne),&
                   ((mallap%mm(j,k),j=1,vpe),k=1,mallap%ne),&
                   ((mallap%nra(j,k),j=1,ape),k=1,mallap%ne),&
                   ((mallap%nrv(j,k),j=1,vpe),k=1,mallap%ne),&
                   ((mallap%z(j,i),j=1,2),i=1,mallap%nver)

endif
close(10)

 CALL calnod(mallap)

END SUBROUTINE leemallap2


SUBROUTINE calnod(mallap2)
! Subrutina que calcula las coordenadas de los nodos de la malla
implicit none
type(malla), INTENT(INOUT)::mallap2
integer::ie,i
!
allocate(mallap2%znod(2,mallap2%n))
do ie=1,mallap2%ne !Bucle en elementos
   do i=1,2
    mallap2%znod(i,mallap2%nn(1,ie))=mallap2%z(i,mallap2%mm(1,ie))
    mallap2%znod(i,mallap2%nn(2,ie))=mallap2%z(i,mallap2%mm(2,ie))
    mallap2%znod(i,mallap2%nn(3,ie))=mallap2%z(i,mallap2%mm(3,ie))
    mallap2%znod(i,mallap2%nn(4,ie))=(mallap2%znod(i,mallap2%nn(1,ie))+mallap2%znod(i,mallap2%nn(2,ie)))/2d0
    mallap2%znod(i,mallap2%nn(5,ie))=(mallap2%znod(i,mallap2%nn(2,ie))+mallap2%znod(i,mallap2%nn(3,ie)))/2d0
    mallap2%znod(i,mallap2%nn(6,ie))=(mallap2%znod(i,mallap2%nn(1,ie))+mallap2%znod(i,mallap2%nn(3,ie)))/2d0
   enddo
enddo
return
end SUBROUTINE calnod


!
!Fin modulo
END module mod_malla 

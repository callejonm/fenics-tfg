#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 28 23:58:42 2019

@author: callejonm
"""
import numpy as np
from scipy.io import FortranFile
from itertools import chain

from fem_to_xml import fem_to_xml

import shutil
#Misma notación que en mod_malla.90

class malla():
    def __init__(self, ne, n, nver, vpe, npe, ape):
        self.ne    = ne
        self.n     = n
        self.nver  = nver
        self.mm    = np.zeros( (vpe, ne) , dtype=int)
        self.nn    = np.zeros( (npe, ne) , dtype=int)
        self.nra   = np.zeros( (ape, ne) , dtype=int)
        self.nrv   = np.zeros( (vpe, ne) , dtype=int)
        self.z     = np.zeros( (2, nver) , dtype=int)
        self.znod  = np.zeros( (2, n) , dtype=int)
    
    
#La malla sera 2D y P1
npe = 6
vpe = 3
ape = 3

#def leemallap(mallap, nmalla, iformat):

 
f = FortranFile('ala_fina', 'r')

(ne, n, nver) = f.read_ints(dtype=np.int32)

print("Elementos", ne)
print("Nodos", n)
print("Vertices", nver)

mallap = malla(ne, n, nver, vpe, npe, ape)

gen = chain( (mallap.nn[j,k]  for j in range(npe) for k in range(ne)   ),
             (mallap.mm[j,k]  for j in range(vpe) for k in range(ne)   ),
             (mallap.nra[j,k] for j in range(ape) for k in range(ne)   ),
             (mallap.nrv[j,k] for j in range(vpe) for k in range(ne) ),
             (mallap.z[j,i]   for j in range(2)   for i in range(nver) ))

dat = f.read_ints(dtype='i4') #MAL; NO LEE LOS REALES DE Z


f.close()
cont = 0
for k in range(ne):
    for j in range(npe):
        mallap.nn[j,k]  = dat[cont] -1
        cont += 1
        
for k in range(ne):
    for j in range(vpe):
        mallap.mm[j,k]  = dat[cont] -1
        cont += 1
        
for k in range(ne):
    for j in range(ape):
        mallap.nra[j,k] = dat[cont]    
        cont += 1
        
for k in range(ne):
    for j in range(vpe):
        mallap.nrv[j,k] = dat[cont]   
        cont += 1
        
for i in range(nver):
    for j in range(2):
        mallap.z[j,i]   = dat[cont]
        cont += 1
        
#def calnod(mallap):
        
mallap = mallap

for ie in range(ne):
    for i in range(2):
        mallap.znod[i, mallap.nn[0,ie]]=mallap.z[i, mallap.mm[0,ie] ]
        mallap.znod[i, mallap.nn[1,ie]]=mallap.z[i, mallap.mm[1,ie] ]
        mallap.znod[i, mallap.nn[2,ie]]=mallap.z[i, mallap.mm[2,ie] ]
        mallap.znod[i, mallap.nn[3,ie]]=(mallap.znod[i,mallap.nn[0,ie]] \
                     +mallap.znod[i,mallap.nn[1,ie]])/2
        mallap.znod[i, mallap.nn[4,ie]]=(mallap.znod[i,mallap.nn[1,ie]] \
                     +mallap.znod[i,mallap.nn[2,ie]])/2
        mallap.znod[i, mallap.nn[5,ie]]=(mallap.znod[i,mallap.nn[0,ie]] \
                     +mallap.znod[i,mallap.nn[2,ie]])/2
                   
def outputFEM2XML(mallap):
    
    out_elem = open("ala_fina_elements.txt", "w+")
     
    for k in range(ne):
        for j in range(vpe):
            out_elem.write(str(mallap.mm[j,k]) + " ")
        out_elem.write("\n")
        
    out_elem.close()
    
    out_nodes = open("ala_fina_nodes.txt", "w+")
           
    for i in range(nver):
        for j in range(2):
            out_nodes.write(str(mallap.znod[j,i]) + " ")
        out_nodes.write("\n")
        
    out_nodes.close()
    
    fem_to_xml("ala_fina")
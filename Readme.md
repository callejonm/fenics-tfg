# N-S FEniCS solver 
### TFG Junio 2019
---

Este proyecto constituye el código Anexo al TFG "Implementación de un solver para las ecuaciones de Navier-Stokes" compresibles presentado en Junio de 2019 en la ETSIAE, UPM. 

Realizado con _Python_ 3.7.2 y _FEniCS_ 2018.1.0. 

Se ha comenzado a implementar dos problemas _test_, que se describen a continuación: 

En **~/sod/** se encuentra el código fuente de la implementación del problema test de Sod. Consta de los siguientes archivos: 

* **sod_problem.py**: función principal del programa. Llama al resto de módulos y realiza la resolución del test.
* **sod\_analytic_press.py**: calcula la solución exacta del problema Sod para el paso de tiempo dado. Tiene implementado un _solver_ de Newton para el cálculo de la presión que se calculó una vez y se fijó. Ha sido el usado.
* **sod\_analytic_mach.py**: igual que el anterior pero se resuelve implicitamente el número de Mach. 
* **matrix_dij.py**: Cálculo de la matriz d_ij del método GMS-LO. 

En **~/fem\_to\_xml/** se encuentran los archivos necesarios para pasar un mallado de formato FEM a formato XML, usado por FEniCS. Se ha usado el código publicado en https://people.sc.fsu.edu/~jburkardt/m\_src/fem\_to\_xml con licencia GNU LGPL.

En **~/naca0012** se encuentran los primeros pasos de la implementación de este método en un perfil NACA0012, que se propone como futuro trabajo. Resaltar de este código la implementación de las condiciones de contorno.
 

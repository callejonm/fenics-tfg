#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:09:51 2019

@author: callejonm
"""

from __future__ import print_function
from fenics import *
from mshr import *
from scipy.sparse import csr_matrix

import ufl as ufl
import numpy as np
import matplotlib as plt
import shutil
import petsc4py
import matrix_dij
import timing
#Borrar graficas anteriores
#shutil.rmtree('graficas/')
#Initial parameters
global t, L


T = .1              # final time
num_steps = 10000       # number of time steps
dt = T / num_steps  # time step size
mu = 0.001          # dynamic viscosity
gamman=1.4
L = 1               #Longitud del mallado: dimensiones Lx0.1L
nx = 20             #Numero de celdas en dirección x
ny = 4              #Numero de celdas en dirección y
delta_x = L/nx
CFL  = dt/delta_x

OPSION = 0          #0 para viscosidad normal, 1 para viscosidad d_ij



inicio = timing.step()

print("\n")
line = "-"*40
if OPSION ==0:
    print(line)
    sim = "Simulacion con viscosidad mu=" + str( mu)
    print(sim)
    caso = "mu"
if OPSION ==1: 
    print(line)
    sim = "Simulacion con viscosidad artificial d_ij"
    print(sim)
    caso = "mud"
    
print("Paso temporal", dt, "(numero de pasos", num_steps, ")")



    
    
#Mallado: generado con fem_to_xml/mod_malla.py
mesh = Mesh("ala_fina.xml")

#Space Function: Sin estabilización, se requiere elementos Galerkin Continuos de orden 2
Qe = FiniteElement('CG', mesh.ufl_cell(), 1)
if OPSION ==0: 
    Ve = VectorElement('CG', mesh.ufl_cell(), 2)
elif OPSION ==1: 
    Ve = VectorElement('CG', mesh.ufl_cell(), 1)

Q = FunctionSpace(mesh, Qe)
V = FunctionSpace(mesh, Ve)

W = FunctionSpace(mesh, MixedElement([Qe, Ve, Qe]))


# Define initial conditions

class InitialCondition(UserExpression):
    def eval_cell(self, values, x, ufc_cell):
        values[0] = 1
        values[1] = 0.0
        values[2] = 0.0
        values[3] = 1.0/(1.4-1.0)  +0.5*0**2/0.125

    def value_shape(self):
        return (4,)
    

#Condiciones de contorno
#Define distintos dominios a partir de mod_malla.f90
#
#    1 : Flujo izquierda             (-4,:)   (inflow)
#    2 : Paredes superior e inferior (:,+-5)  (walls)
#    3 : Flujo derecha               (10, :) (outflow)
#    4 : Perfil
#
#
#       
        
sub_domains = MeshFunction('size_t', mesh, 1)
sub_domains.set_all(0)

for cell in cells(mesh):
    i    = cell.global_index()
    arista = cell.entities(1)
    
    for j in range(3):
        sub_domains[arista[j]] = mallap.nra[j,i]
        
        #if sub_domains[arista[j]] == 2:
        #    print(cell.get_vertex_coordinates())

walls = 'near(x[1], -5.0) || near(x[1], +5.0)'
inflow = 'near(x[0], -4.0)'

no_slip = Constant( (0.0, 0.0) )
inflow_mom  = Constant( (1.0, 0.0) )



bc_inflow    = DirichletBC(W.sub(1), inflow_mom, inflow  )
bc_walls     = DirichletBC(W.sub(1), inflow_mom, walls  )
bc_airfoil   = DirichletBC(W.sub(1), no_slip, sub_domains, 4  )

bc = [bc_inflow, bc_walls, bc_airfoil]



# Define functions for solutions at previous (_n) and current (_)time steps and assign initial conditions
w = TrialFunction(W)
(r_test, m_test, s_test)=TestFunctions(W)
w_ = Function(W)
w_n = Function(W)

# Define expressions used in variational forms
w_n2 = .5*(w + w_n)
n  = FacetNormal(mesh)
f  = Constant((0, 0))
k  = Constant(dt)
mu = Constant(mu)
gamma = Constant(gamman)

def splitV(w):     # Función para visualización y output de datos, no permite calculos posteriores con r,u o p
    (r, m , s)=w.split()
    u=m/r
    p=(gamma-1)*(s-dot(m,m)/2/r)
    return (r,u,p)

def ncons2cons(rho,u, p):     
    m=rho*u
    s=p/(gamma-1) + 0.5*dot(u,u)*rho
    return (rho,m,s)

def cons2ncons(rho,m,s):     
    u=m/rho
    p=(gamma-1)*(s-dot(m,m)/2/rho)
    return (rho,u,p)

def tau(mu,m,r):
    i,j=ufl.indices(2)
    u=m/r
    tau_u_ij= mu*(grad(u)[i,j]+grad(u)[j,i])-2/3*mu*div(u)*Identity(len(u))[i,j]
    tau_u = as_tensor(tau_u_ij, (i,j))
    
    tau_m_ij=tau_u[i,j]*r+mu/r*((m[i]*grad(r)[j]+m[j]*grad(r)[i])-2/3*dot(m,grad(r))*Identity(len(u))[i,j])
    tau_m = as_tensor(tau_m_ij, (i,j))
    
    return tau_m
  

# Apply boundary conditions to matrices
#[bc.apply(A1) for bc in bcu]
#[bc.apply(A2) for bc in bcp]


# Save mesh to file (for use in reaction_system.py)
#File('navier_stokes_sod/cylinder.xml.gz') << mesh

dir_grafs = "graf_"+ caso + "_" + str(nx) + "_" + str(num_steps) + "/"
fin_pvd   =     "_"+ caso + "_" + str(nx) + ".pvd"


filename_r  =  dir_grafs + "dens" + fin_pvd
filename_m  =  dir_grafs + "mome" + fin_pvd
filename_s  =  dir_grafs + "ener" + fin_pvd

file_r = File( filename_r) 
file_m = File( filename_m)
file_s = File( filename_s)

# Apply initial condition: no boundary conditions applied in Sod problem
u_init = InitialCondition()
w_n.interpolate(u_init)
w_.interpolate(u_init)

(r,m,s)    = split(w)
(r_n,m_n,s_n) = w_n.split()

#(r,u,p)=splitV(w)
(r_n,u_n,p_n)=splitV(w_n)


t = 0.0
stepcounter = 0.0
#%%
file_r.write(w_.split()[0], stepcounter)
file_m.write(w_.split()[1], stepcounter)
file_s.write(w_.split()[2], stepcounter)

error = np.zeros((num_steps,nx+1))

VECTOR_NODAL = w_.vector().vec().array

#%% Bucle temporal

if OPSION ==0: 
    
    
    F1= dot(r_test, (r-r_n)/k)*dx - dot(grad(r_test), m_n)*dx      + dot(r_test, dot(m_n,n))*ds   
        
    F2= dot(m_test, (m-m_n)/k)*dx - inner(grad(m_test), outer(m_n,m_n)/r_n + ((gamma-1)*(s_n-dot(m_n,m_n)/2/r_n))*Identity(2))*dx \
    +  dot(m_test, m_n/r_n*dot(m_n,n) +((gamma-1)*(s_n-dot(m_n,m_n)/2/r_n))*n)*ds \
    +    mu*inner(grad(m_n/r_n),grad(m_test))*dx   - mu*inner(m_test ,dot( grad( m_n/r_n) , n))*ds #Termino viscoso
    
    F3 = dot(s_test, (s-s_n)/k)*dx -dot(grad(s_test), (s_n+((gamma-1)*(s_n-dot(m_n,m_n)/2/r_n)))*m_n/r_n)*dx +dot(s_test, (s_n+((gamma-1)*(s_n-dot(m_n,m_n)/2/r_n)))/r_n*dot(m_n,n))*ds
    
    F=F1+F2+F3
    
    a=lhs(F)
    Le=rhs(F)
    
    # Ensamblaje de matriz A (izquierda del sistema lineal)
    A_uv=assemble(a)  
    for step in range(num_steps):
       

        b_v = assemble(Le)
        
        #Se aplican condiciones de contorno
        [bc_i.apply(A_uv,  b_v) for bc_i in bc]

        solve(A_uv, w_.vector(), b_v)        
        
        # Update current time
        t += dt
    
        
        #Posibilidad de hacer plots dentro de las funciones integradas de FEnics -> Se elige Paraview
      #  plot(r_, title='Density')
      #  plot(u_, title='Velocity')
      #  plot(p_, title='Pressure')
        stepcounter+= 1;
        
        # Save solution to file (pvd)
        file_r.write(w_.split()[0], stepcounter)    
        file_m.write(w_.split()[1], stepcounter)
        file_s.write(w_.split()[2], stepcounter)
        
        # Update previous solution
        (r_,m_,s_) = w_.split(deepcopy = True)
        r_n.assign(r_)
        m_n.assign(m_)
        s_n.assign(s_)
    
        # Update progress bar
        progress=t / T*100

        #Output: error máximo en momento
        vertex_values = w_.sub(1).compute_vertex_values()
      #  if progress % 1 <= 100/num_steps:
      
        line = "-"*40
        print("PROGRESO:", progress, "%")
        #print(s_.vector().max())
        print('Valor max:', vertex_values.max())
        print(line)
        timing.log('')
            
elif OPSION ==1:
    
    #Precalculo para matriz d
    MatC1, MatC2 = matrix_dij.MatC(W)
    
    #Matriz de grados de libertad de todos los vertices
    DOF   = np.array([W.sub(0).dofmap().entity_dofs(mesh,0),
                      W.sub(1).sub(0).dofmap().entity_dofs(mesh,0),
                      W.sub(1).sub(1).dofmap().entity_dofs(mesh,0),       
                      W.sub(2).dofmap().entity_dofs(mesh,0)])         
    idx = np.ix_(DOF[0], DOF[0])
    #Matriz de grados de libertad de todos las aristas
    #DOF_1 = np.array([W.sub(1).sub(0).dofmap().entity_dofs(mesh,1),
    #                  W.sub(1).sub(1).dofmap().entity_dofs(mesh,1)  ]) 
    #    
    
    for step in range(num_steps):
 
       # Formulación UPC
       # F1 = dot( r_test, (r-r_n)/k)*dx + dot(r_test, div(m_n))*dx 
          
       # F2 =dot( m_test, (m-m_n)/k)*dx + dot(m_test, -m_n/r_n**2*dot(m_n, grad(r_n)) + (gamma-1)* dot(m_n,m_n)/2/r_n**2*grad(r_n) + m_n/r_n*div(m_n)
       # +1/r_n*nabla_grad(m_n)*m_n - (gamma-1)/r_n*grad(m_n)*m_n + (gamma-1)*grad(s_n))*dx +\
       # -inner(-tau(mu,m_n,r_n),grad(m_test))*dx + inner(m_n,dot(-tau(mu,m_n,r_n), n))*ds
    
       # F3 = dot( s_test, (s-s_n)/k -(s_n+p_n)/r_n**2*dot(m_n,grad(r_n)) +(gamma-1)*dot(m_n,m_n)/2/r_n**3*dot(m_n,grad(r_n))+\
       # (s_n+p_n)/r_n*div(m_n) -(gamma-1)*inner(outer(m_n,m_n),grad(m_n)) + gamma*dot(m_n/r_n,grad(s_n)))*dx +\
       # -dot(-tau(mu,m_n,r_n)*m_n/r_n,grad(s_test))*dx \
        
        F1= dot(r_test, (r-r_n)/k)*dx - dot(grad(r_test), m_n)*dx + dot(r_test, dot(m_n,n))*ds   
        
        F2= dot(m_test, (m-m_n)/k)*dx - inner(grad(m_test), outer(m_n,m_n)/r_n + p_n*Identity(2))*dx \
        +  dot(m_test, m_n/r_n*dot(m_n,n) +p_n*n)*ds \
       # +    mu*inner(grad(m_n/r_n),grad(m_test))*dx  #Termino viscoso
        
        F3 = dot(s_test, (s-s_n)/k)*dx -dot(grad(s_test), (s_n+p_n)*m_n/r_n)*dx +dot(s_test, (s_n+p_n)/r_n*dot(m_n,n))*ds
        
        F=F1+F2+F3
        
        a=lhs(F)
        Le=rhs(F)
        
        # Resolver de forma matricial: al tener polinomios de lagrange orden 1 para todo
        # coindicen vértices y nodos. 
        
        A_uv=assemble(a)            
        b_v = assemble(Le)
        
        
        #Se aplican condiciones de contorno
        [bc_i.apply(A_uv,  b_v) for bc_i in bc]
        
        
        # Al vector b_v se le añaden termino de  viscosidad artificial d_ij*u_n)
        if step==0:
            row,colum,val = as_backend_type(A_uv).mat().getValuesCSR()                               
            S = csr_matrix((val,colum,row))
            S.eliminate_zeros()
            
        dij, dmax = matrix_dij.petsc(mesh, W, MatC1, MatC2, DOF, S, w_n, t) #obtiene d_ij
                
        b_d = dij * w_.vector().vec()
        b_v.add_local(b_d)

        print("d maxima = ", dmax)
        
        solve(A_uv, w_.vector(), b_v)        
        # Update current time
        t += dt
    
        (r_,m_,s_)=w_.split()
        
        #Posibilidad de hacer plots dentro de las funciones integradas de FEnics -> Se elige Paraview
      #  plot(r_, title='Density')
      #  plot(u_, title='Velocity')
      #  plot(p_, title='Pressure')
        stepcounter+= 1;
        
        # Save solution to file (pvd)
        file_r.write(w_.split()[0], stepcounter)    
        file_m.write(w_.split()[1], stepcounter)
        file_s.write(w_.split()[2], stepcounter)
        
        # Update previous solution
        r_n = project(r_, Q)
        m_n = project(m_, V)
        s_n = project(s_, Q)
        p_n=(gamma-1)*(s_n-dot(m_n,m_n)/2/r_n)
    
        # Update progress bar
        progress=t / T*100
        
        #Output: error máximo en momento
        vertex_values = w_.sub(1).compute_vertex_values()
      #  if progress % 1 <= 100/num_steps:
      
        line = "-"*40
        print("PROGRESO:", progress, "%")
        #print(s_.vector().max())
        print('Valor max:', vertex_values.max())
        print(line)
        timing.log('')
        

#%% Comparate with exact solution: in a FEniCS function and pvd file.


tiempo = timing.steplog(inicio, "Final de simulación")

file_temp = open("tcomp.txt","a")
tcomp = "Tiempo de computacion para " + sim + ", mallado " + str(nx)+"x"+str(ny)\
 + " CFL = " + str(CFL) +" :    " + str(tiempo) + "segundos  " + " \n"
file_temp.write(tcomp) 
file_temp.close()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  8 23:50:53 2019

@author: callejonm
"""

#python3 
import atexit 
from time import time, strftime, localtime 
from datetime import timedelta 

def secondsToStr(elapsed=None): 
    if elapsed is None: 
     return strftime("%Y-%m-%d %H:%M:%S", localtime()) 
    else: 
     return str(elapsed) + " segundos"

def log(s, elapsed=None): 
    line = "="*40 
    print(line) 
    print(secondsToStr(), '-', s) 
    if elapsed: 
     print("Elapsed time:", elapsed) 
    print(line) 
    print() 

def endlog(): 
    end = time() 
    elapsed = end-start 
    log("End Program", secondsToStr(elapsed)) 

def step():
    step = time()
    return step

def steplog(step, text):
    end = time()
    elapsed = end-start
    log(text, secondsToStr(elapsed))
    return elapsed

start = time() 
atexit.register(endlog) 
log("Start Program") 
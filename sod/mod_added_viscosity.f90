MODULE mod_added_viscosity

USE mod_malla
USE mod_morse
USE mod_mcambio





CONTAINS




SUBROUTINE Dmatrix(malla_ns, veloA, u,fun, cambioe)
      
USE mod_variables_mov, ONLY: matC1, matC2, matD, matD_HO, iHO, iCL, method_HO ,nrvg, gamma, mat_mass        
IMPLICIT NONE
TYPE(malla),                                    INTENT(IN) :: malla_ns
REAL(KIND=8) , ALLOCATABLE, DIMENSION(:,:),     INTENT(IN) :: veloA, u
TYPE(cambio_noafin), ALLOCATABLE, DIMENSION(:), INTENT(IN) :: cambioe

!====

REAL(KIND=8) :: cij(2), cji(2), nij(2), nji(2),  normij, normji,&
         lambda1, lambda2,  sum,x(2), cl, cr, vel_eul_r, vel_eul_l, pre_l, pre_r, &
          sum1, sum2, maxi
INTEGER:: i, j, col, l, indice

REAL(KIND=8) , ALLOCATABLE, DIMENSION(:)::fun, alfa, pre, matBeta
LOGICAL:: flux_convj, flux_convi



if(.not.allocated(matD)) allocate(matD(size(mat_mass%A)))


CALL Dmatrix_low(malla_ns, veloA, u, cambioe, matD)


       

IF(iHO)THEN
  
  if(.not.allocated(matD_HO)) allocate(matD_HO(size(mat_mass%A)))
  matD_HO=matD
  
  IF(method_HO==1)THEN
     allocate(matBeta(size(mat_mass%A)), fun(malla_ns%nver), alfa(malla_ns%nver))

     !CALL compute_betaij(malla_ns,matBeta)
     
     CALL compute_betaij_1(malla_ns,matBeta, cambioe)

     fun=0d0
     
     
     DO i=1,malla_ns%nver
 
        sum1=0d0; sum2=0d0; maxi= u(i,1) !maxi=pre(i)   !maxi=rho(i)
       
        DO j=mat_mass%mu1(i),mat_mass%mu1(i+1)-1
           col=mat_mass%mu2(j)
           if(i.ne.col)then
             sum1 = sum1+ matBeta(j)*(u(col,1)-u(i,1))
             sum2 = sum2+ abs(matBeta(j))*abs((u(col,1)-u(i,1)))
             
             !sum1 = sum1+ matBeta(j)*(pre(col)-pre(i))
             !sum2 = sum2+ matBeta(j)*abs(pre(col)-pre(i))
      
             maxi = max(maxi,u(col,1))
             !maxi = max(maxi,pre(col))
           endif
        ENDDO
      
       
       alfa(i) = abs(sum1)/max(sum2,maxi*1d-8)
 
       
        if(alfa(i) > 1d0 .or. alfa(i)<0d0.or.isnan(alfa(i))) then
           print*,'error en la viscosidad orden 2', alfa(i),i, sum1, sum2, pre(i)
           stop
        endif
    
        fun(i)=alfa(i)**4
     
     ENDDO


     DO i=1,malla_ns%nver
        sum=0d0
        
        DO j=mat_mass%mu1(i),mat_mass%mu1(i+1)-1
           col=mat_mass%mu2(j)
           if(i.ne.col)then
              matD_HO(j)= matD(j)*max(fun(i),fun(col))
              sum=sum+matD_HO(j)
           else
              indice=j
           endif
        ENDDO
        matD_HO(indice)= -sum
     ENDDO

     deallocate(alfa)

   ELSEIF(method_HO==2)THEN

     CALL entropy_viscosity(malla_ns, matD, matD_HO,matC1, matC2, veloA, u)
  
   ELSEIF(method_HO==3)THEN !PURE GALERKIN
      matD_HO=0d0
   ENDIF
ENDIF


IF(iHO.and.not(iCL))THEN
   !print*,'NO hay CL pero la viscosidad es la de alto orden'
   matD=matD_HO
ENDIF
RETURN
END SUBROUTINE Dmatrix





SUBROUTINE Dmatrix_low(malla_ns, veloA, u, cambioe, Dlow)
      
USE mod_variables_mov, ONLY: matC1, matC2, nrvg, gamma, mat_mass       
 
IMPLICIT NONE
TYPE(malla),                                INTENT(IN) :: malla_ns
REAL(KIND=8) , ALLOCATABLE, DIMENSION(:,:), INTENT(IN) :: veloA, u
REAL(KIND=8) , ALLOCATABLE, DIMENSION(:),   INTENT(OUT) :: Dlow
TYPE(cambio_noafin), ALLOCATABLE, DIMENSION(:), INTENT(IN) :: cambioe
!====
REAL(KIND=8) :: cij(2), cji(2), nij(2), nji(2),  normij, normji,&
         lambda1, lambda2,  sum,x(2), cl, cr, vel_eul_r, vel_eul_l, pre_l, pre_r, &
          sum1, sum2, maxi
INTEGER:: i, j, col, l, indice




if(.not.allocated(Dlow)) allocate(Dlow(size(mat_mass%A)))

Dlow=0d0

DO i=1,malla_ns%nver
   x=malla_ns%z(:,i)
   sum=0d0
   DO j=mat_mass%mu1(i),mat_mass%mu1(i+1)-1
       col=mat_mass%mu2(j)
       if(i.ne.col)then
          cij(1)=matC1(j)
          cij(2)=matC2(j)

          ! La componente simetrica 
           DO l=mat_mass%mu1(col), mat_mass%mu1(col+1)-1
              IF(mat_mass%mu2(l)==i)THEN
                 cji(1)=matC1(l)
                 cji(2)=matC2(l)
                 EXIT
              ENDIF
           ENDDO

           normij=sqrt(cij(1)**2+cij(2)**2)
           normji=sqrt(cji(1)**2+cji(2)**2)
           nij(1:2)=cij(1:2)/normij
           nji(1:2)=cji(1:2)/normji
           ! Euler 
           vel_eul_l = dot_product(u(i,2:3)/u(i,1) , nij) 
           vel_eul_r = dot_product(u(col,2:3)/u(col,1), nij) 
           pre_l = max((gamma-1d0) * (u(i,4)-0.5d0*u(i,1)*vel_eul_l**2),1d-15)
           pre_r = max((gamma-1d0) * (u(col,4)-0.5d0*u(col,1)*vel_eul_r**2),1d-15)
           cl = sqrt(gamma*pre_l/u(i,1))
           cr = sqrt(gamma*pre_r/u(col,1))
           
           CALL compute_lambda(vel_eul_r ,vel_eul_l, cr, cl, nij,lambda1,veloA(:,col))
        
      
           vel_eul_l = dot_product(u(i,2:3)/u(i,1) , nji) 
           vel_eul_r = dot_product(u(col,2:3)/u(col,1), nji) 
           pre_l = max(1d-15,(gamma-1d0) * (u(i,4)-0.5d0*u(i,1)*vel_eul_l**2))
           pre_r = max((gamma-1d0) * (u(col,4)-0.5d0*u(col,1)*vel_eul_r**2),1d-15)
           cl = sqrt(gamma*pre_l/u(i,1))
           cr = sqrt(gamma*pre_r/u(col,1))

           CALL compute_lambda(vel_eul_l,vel_eul_r, cl, cr,nji,lambda2,veloA(:,i) )
          
            
           Dlow(j)=max(lambda1*normij, lambda2*normji)
           
           if(Dlow(j)<0d0)then
              print*,'No es Dij >0'
              stop
           endif

           sum=sum+Dlow(j)
        else
           indice=j
        endif

        
   ENDDO
   
    Dlow(indice)=-sum

ENDDO

RETURN
END SUBROUTINE Dmatrix_low




SUBROUTINE compute_lambda(ur,ul, cr, cl,nij,lambda, vel)
IMPLICIT NONE

REAL(KIND=8), INTENT(OUT) :: lambda
REAL(KIND=8), INTENT(IN)  :: nij(2), vel(2)
REAL(KIND=8)              :: ur, ul, cr, cl
REAL(KIND=8)              :: wn
 


wn     = dot_product(vel,nij)


lambda = max(abs(ur-wn)+cr, abs(ul-wn)+cl)



RETURN
END SUBROUTINE compute_lambda






SUBROUTINE compute_betaij(mallap,matBeta)
USE mod_variables_mov, ONLY: nrvg, mat_mass
IMPLICIT NONE
TYPE(malla):: mallap
REAL(KIND=8) :: matBeta(size(mat_mass%A))
INTEGER :: m, ni, n1, n2, i, j, i1, i2, p
REAL(KIND=8) :: d1, d2, alpha, scal, x
REAL(KIND=8), DIMENSION(2) :: xi, x1, x2


matBeta = 0d0 


DO m = 1, mallap%ne
   DO ni = 1, 3
      
      n1 = MODULO(ni,3)+1
      n2 = MODULO(ni+1,3)+1

      i  = mallap%mm(ni,m)
      i1 = mallap%mm(n1,m)
      i2 = mallap%mm(n2,m)
      
      xi = mallap%z(:,i)
      x1 = mallap%z(:,i1)-xi
      x2 = mallap%z(:,i2)-xi
      d1 = SQRT(SUM(x1**2))
      d2 = SQRT(SUM(x2**2))
      scal = SUM(x1*x2)
      alpha = ACOS(scal/(d1*d2))
      DO p = mat_mass%mu1(i), mat_mass%mu1(i+1) - 1
             IF (mat_mass%mu2(p)==i1) THEN
                matBeta(p) = matBeta(p) + TAN(alpha/2)/d1
             ELSE IF (mat_mass%mu2(p)==i2) THEN
                matBeta(p) = matBeta(p) + TAN(alpha/2)/d2
             END IF
          END DO
       END DO
    END DO
    DO i = 1, mallap%nver
       x = 0.d0
       DO p = mat_mass%mu1(i),mat_mass%mu1(i+1) - 1
          j = mat_mass%mu2(p)
          if ( matBeta(p)==0.d0 .AND. i.ne.j) THEN
             write(*,*) ' BUG', matBeta(p), p, i, j
             stop
          end if
            x = x + matBeta(p)*(mallap%z(1,j) - mallap%z(1,i))
      END DO
      if(abs(x)>1e-10.and.nrvg(i)==0) then
        stop
      endif
    END DO
 
END SUBROUTINE compute_betaij


SUBROUTINE compute_betaij_1(malla_ns,matBeta, cambioe)
USE mod_variables_mov, ONLY: nrvg, mat_mass, DP1
IMPLICIT NONE
TYPE(malla):: malla_ns
TYPE(cambio_noafin), ALLOCATABLE, DIMENSION(:), INTENT(IN) :: cambioe
REAL(KIND=8) :: matBeta(size(mat_mass%A))
INTEGER :: i, j,k, v1, v2, ll




matBeta = 0d0 


DO k=1,malla_ns%ne
     
      
      DO i=1,3
         v1=malla_ns%mm(i,k)
         DO j=1,3
            v2=malla_ns%mm(j,k)
             DO ll=mat_mass%mu1(v1),mat_mass%mu1(v1+1)-1
               IF (v2==mat_mass%mu2(ll)) then
                   matBeta(ll)=matBeta(ll)+dot_product(DP1(:,i),DP1(:,j))*cambioe(k)%area
               ENDIF
            ENDDO
          ENDDO
        ENDDO
  ENDDO


 
END SUBROUTINE compute_betaij_1






SUBROUTINE entropy_viscosity(malla_ns, matD, matD_HO, matC1, matC2, VELOA, u)
USE mod_variables_mov, ONLY: gamma, mat_mass, nrvg, iEV, iopc_EV
IMPLICIT NONE
TYPE(malla), INTENT(IN) :: malla_ns
REAL(KIND=8), ALLOCATABLE, DIMENSION(:), INTENT(IN):: matD, matC1, matC2
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:), INTENT(IN):: veloA, u

REAL(KIND=8), ALLOCATABLE, DIMENSION(:), INTENT(INOUT)::  matD_HO


INTEGER :: i, j, col, indice
REAL(KIND=8) :: sum, eps, sum1, sum2, sum1_ale,sum2_ale, factor, &
                eta_p(4), norma=0d0, norm0, norm1, norm2, norm3, norm4, &
                norm1_ale, norm2_ale, norm3_ale, norm4_ale, norm0_ale, entro_aux, ener
REAL(KIND=8) ,ALLOCATABLE, DIMENSION(:)   ::  R, pre, etamax, etamin, entropy
REAL(KIND=8) ,ALLOCATABLE, DIMENSION(:,:) ::  velo

character*100::nombre
ALLOCATE(R(malla_ns%nver), pre(malla_ns%nver),velo(2,malla_ns%nver), etamax(malla_ns%nver), etamin(malla_ns%nver), entropy(malla_ns%nver))

! Calculamos eta(i)



! Primera opcion
do i=1,malla_ns%nver
  velo(1:2,i)   = u(i,2:3)/u(i,1)
  pre(i)    = max((gamma-1d0) *(u(i,4) - 0.5d0*(velo(1,i)**2+velo(2,i)**2)*u(i,1)),1d-15)
  
  
  IF(iopc_EV==1)then
    entropy(i) = pre(i)**(1d0/gamma)
  ELSE
    !  entropy(i) = u(i,1) *(1d0/(gamma-1d0)*log(u(i,4)-(u(i,2)**2+u(i,3)**2)/u(i,1)*0.5d0)-gamma/(gamma-1d0)*log(u(i,1)))
    entropy(i) = u(i,1) * ( 1d0/(gamma-1d0)*log(pre(i)/(gamma-1d0)) - gamma/(gamma-1d0)*log(u(i,1)) )
  ENDIF

  if(isnan(velo(1,i)).or.isnan(velo(2,i)).or.isnan(pre(i)).or.isnan(entropy(i)))then
   print*,'La velo mal en r(i)',i,u(i,1:4),pre(i),velo(1:2,i), entropy(i)
  endif
enddo


do i=1,malla_ns%nver
  !etamax(i) = 0d0
  etamax(i)=entropy(i)
  etamin(i)=entropy(i)
  do j=mat_mass%mu1(i),mat_mass%mu1(i+1)-1
     col = mat_mass%mu2(j)
     if(col/=i)then
     etamax(i)    = max(entropy(col),etamax(i))
     etamin(i)    = min(entropy(col),etamin(i))
     if(isnan(etamax(i)).or.etamax(i)>1d10)then
       print*,'La entropia eta max y etamin son NAN', etamax(i),i,entropy(i)
       stop
     endif
     endif
  enddo
  
enddo




  
   do i=1,malla_ns%nver
      !Calculamos eta'(i)*f(i)
      IF(iopc_EV==1)then

         factor = (gamma-1d0)/gamma*pre(i)**(1d0/gamma-1d0)
         if(iEV ) then   
           !Relative
           eta_p(1) = 0.5d0*(velo(1,i)**2+velo(2,i)**2)*factor-entropy(i)/u(i,1)
         else
           eta_p(1) = 0.5d0*(velo(1,i)**2+velo(2,i)**2)*factor
         endif
         eta_p(2) = -velo(1,i)*factor
         eta_p(3) = -velo(2,i)*factor
         eta_p(4) = factor
      ELSEIF(iopc_EV==2)then
         factor = 1d0/(gamma-1d0)
         ener = u(i,4)/u(i,1) - 0.5d0*(velo(1,i)**2+velo(2,i)**2)
         if(iEV ) then   
          !Relative
          !print*,'hola'
           eta_p(1) = entropy(i) + factor*(0.5d0*(velo(1,i)**2+velo(2,i)**2)/ener - gamma)-entropy(i)/u(i,1)
         else
           eta_p(1) = entropy(i) + factor*(0.5d0*(velo(1,i)**2+velo(2,i)**2)/ener - gamma)
         endif
         eta_p(2) = -velo(1,i)/ener*factor
         eta_p(3) = -velo(2,i)/ener*factor
         eta_p(4) = factor/ener
      ENDIF

      sum=0d0; norm1=0d0; norm2=0d0; norm3=0d0; norm4=0d0; norm0=0d0
      norm1_ale=0d0; norm2_ale=0d0; norm3_ale=0d0; norm4_ale=0d0; norm0_ale=0d0

      do j=mat_mass%mu1(i),mat_mass%mu1(i+1)-1
      
         col = mat_mass%mu2(j)
         if(iEV)then
            entro_aux = entropy(col)-u(col,1)/u(i,1)*entropy(i)
         else
            entro_aux = entropy(col)
         endif

         sum1 = velo(1,col)*entro_aux-( u(col,2)*eta_p(1)+(velo(1,col)* u(col,2)+pre(col))*eta_p(2)&
           + (velo(2,col)* u(col,2))*eta_p(3)+(velo(1,col)*( u(col,4)+pre(col)))*eta_p(4))

         sum2 = velo(2,col)*entro_aux-(u(col,3)*eta_p(1)+(velo(1,col)*u(col,3))*eta_p(2)&
            + (velo(2,col)*u(col,3)+pre(col))*eta_p(3)+(velo(2,col)*( u(col,4)+pre(col)))*eta_p(4))
      
         sum1_ale = u(col,1)*veloA(1,i)*eta_p(1)+  u(col,2)*veloA(1,i)*eta_p(2)+&
               +u(col,3)*veloA(1,i)*eta_p(3)+ u(col,4)*veloA(1,i)*eta_p(4)-entro_aux*veloA(1,i)
 
         sum2_ale = u(col,1)*veloA(2,i)*eta_p(1)+ u(col,2)*veloA(2,i)*eta_p(2)+&
               +u(col,3)*veloA(2,i)*eta_p(3)+ u(col,4)*veloA(2,i)*eta_p(4)-entro_aux*veloA(2,i)
     
     
         sum = sum + sum1_ale*matC1(j) + sum2_ale*matC2(j) &
             + sum1*matC1(j) + sum2 *matC2(j)


         norm0 = norm0 + (velo(1,col))*entro_aux *matC1(j)+ (velo(2,col))*entro_aux *matC2(j) 
         norm0_ale=norm0_ale + veloA(1,i)*entro_aux *matC1(j)+veloA(2,i)*entro_aux *matC2(j) 

         norm1 = norm1 + (u(col,2)-u(col,1)*veloA(1,i) )*matC1(j)+(u(col,3)-u(col,1)*veloA(2,i))*matC2(j)

         norm2 = norm2 + (velo(1,col)* u(col,2)+pre(col))*matC1(j) &
                    + (velo(1,col)* u(col,3))*matC2(j)
      
         norm2_ale = norm2_ale +u(col,2)*veloA(1,i)*matC1(j) + u(col,2)*veloA(2,i)*matC2(j)
      
         norm3 = norm3 + (velo(2,col)* u(col,2))*matC1(j) &
                    + (velo(2,col)* u(col,3)+pre(col))*matC2(j)
                    
         norm3_ale=norm3_ale+ u(col,3)*veloA(1,i)*matC1(j) + u(col,3)*veloA(2,i)*matC2(j)
      
      
         norm4 = norm4 + (velo(1,col)*( u(col,4)+pre(col)))*matC1(j) &
                   + (velo(2,col)*( u(col,4)+pre(col))) *matC2(j)
                   
         norm4_ale=norm4_ale+u(col,4)*veloA(1,i)*matC1(j)+u(col,4)*veloA(2,i) *matC2(j)
       
     

     enddo
 

   
   norma= abs(norm0)+abs(norm0_ale) + (abs(norm1)+abs(norm1_ale))*abs(eta_p(1))&
                     + (abs(norm2)+abs(norm2_ale))*abs(eta_p(2))&
                     + (abs(norm3)+abs(norm3_ale))*abs(eta_p(3))&
                     + (abs(norm4)+abs(norm4_ale))*abs(eta_p(4))
   

    
  
    
    if(iEV)then
       
        R(i) = abs(sum)/norma
       if(norma<1d-14) R(i)=0d0

     if(isnan(R(i)).or. R(i)>1d0+1d-10.or.R(i)<-1d-10)then
        print*,'R(i) es NAN',R(i),i, sum,norma, abs(norm1),abs(norm2),abs(norm3),abs(norm4), abs(eta_p(1:4))
       stop

     endif
    else
       
       eps  = 1d-8*max(abs(etamax(i)),abs(etamin(i)))
       R(i) = abs(sum) /max(abs(etamax(i)-etamin(i)),eps)
   
       if(norma<1d-16) R(i)=0d0
    endif
enddo



matD_HO = 0d0

do i = 1,malla_ns%nver
   sum=0d0
   do j = mat_mass%mu1(i),mat_mass%mu1(i+1)-1
      col= mat_mass%mu2(j)
      if( i.ne.col)then
         if(iEV) then
           matD_HO(j) =matD(j)*max(R(i),R(col)) 
         else 
          matD_HO(j) =min(matD(j),max(abs(R(i)),abs(R(col)))) 
         endif
         sum=sum+matD_HO(j)
      else
         indice = j
      endif
   enddo
 
   matD_HO(indice) = -sum

enddo

END SUBROUTINE entropy_viscosity


END MODULE mod_added_viscosity

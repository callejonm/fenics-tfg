// Gmsh project created on Sat Apr  6 17:52:18 2019

L = 10;
gridsize = L/20;
meshThickness = L/100;

Point(1) = {0, 0, 0, gridsize};
Point(2) = {L, 0, 0, gridsize};
Point(3) = {L, 0.1*L, 0, gridsize};
Point(4) = {0, 0.1*L, 0, gridsize};

Line(1) = {1, 2};				// bottom line
Line(2) = {2, 3};				// right line
Line(3) = {3, 4};				// top line
Line(4) = {4, 1};				// left line
	
Line Loop(5) = {1, 2, 3, 4}; 	
// the order of lines in Line Loop is used again in surfaceVector[]
Plane Surface(6) = {5};

//Transfinite surface, structured mesh:
Transfinite Surface {6};

surfaceVector[] = Extrude {0, 0, 0} {
	Surface{6};
	Layers{1};
	Recombine;
};
/* surfaceVector contains in the following order:
[0]	- front surface (opposed to source surface)
[1] - extruded volume
[2] - bottom surface (belonging to 1st line in "Line Loop (6)")
[3] - right surface (belonging to 2nd line in "Line Loop (6)")
[4] - top surface (belonging to 3rd line in "Line Loop (6)")
[5] - left surface (belonging to 4th line in "Line Loop (6)") */
Physical Surface("front") = surfaceVector[0];
Physical Volume("internal") = surfaceVector[1];
Physical Surface("back") = {6}; // from Plane Surface (6) ...






#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 16:58:13 2019

@author: callejonm
"""

import numpy as np
from scipy.optimize import fsolve

def sod_func(Ms):
    #defines function to be used in analytic_sod
    #Initial conditions
    rho_l = 1
    P_l = 1
    u_l = 0

    rho_r = 0.125
    P_r = 0.1
    u_r = 0

    gamma = 1.4

    mu = pow( ((gamma-1)/(gamma+1)) , 0.5)
    a_l = pow( (gamma*P_l/rho_l),0.5)
 
    #   y = (P - P_r)*(( ((1 - mu**2)**2)*((rho_r*(P + mu*mu*P_r))**(-1)) )**(0.5)) \
    #  - 2*(pow(gamma, 0.5)/(gamma - 1) )*(1 - pow(P, (gamma - 1)/(2*gamma)))
    
    y = Ms - 1/Ms - a_l*(gamma+1)/(gamma-1)*( 1 - pow( (P_r/P_l * (2*gamma/(gamma+1)*Ms**2 - (gamma-1)/(gamma+1))) , (gamma-1)/(2*gamma)  ))  
    return y

    

def solve(t, L, n_points):
        
    if t < 0.01:
        t=0.01
        
    #Initial conditions 
    x0 = 0.5*L
    rho_l = 1
    P_l = 1
    u_l = 0
    L = L  
    
    rho_r = 0.125
    P_r = 0.1
    u_r = 0
    
    gamma=1.4
    #mu = pow( ((gamma-1) / (gamma+1)) , 0.5)
    mu= 0.001
    
    #Speed of sound
    a_l = pow( (gamma*P_l/rho_l),0.5)
    a_r = pow( (gamma*P_r/rho_r),0.5)
    
    
    Ms = fsolve(sod_func, 2)
    
    P_1   = P_r * (2*gamma/(gamma+1)*Ms**2-(gamma-1)/(gamma+1))
    rho_1 = rho_r / (2/(gamma+1)/Ms**2 +(gamma-1)/(gamma+1))
    u_1 = 2/(gamma+1)*(Ms-1/Ms)
    
    P_2 = P_1
    u_2 = u_1
    rho_2 = pow((P_2/P_l), 1/gamma) * rho_l
    a_2   = a_l - u_2*(gamma-1)/2
    
    class data:
        #Key Positions
        x1 = x0 - a_l*t
        x3 = x0 + u_2*t
        x4 = x0 + Ms*t
        #determining x2
        x2 = x0 + (u_2 - a_2)*t
        
        #start setting values
            #set by user
        #boundaries (can be set)
        x_min = 0
        x_max = L
    
    
        
        x = np.linspace(x_min,x_max,n_points)
        rho = np.zeros((n_points,1))
        P = np.zeros((n_points,1))
        u = np.zeros((n_points,1))
        e = np.zeros((n_points,1))
        m = np.zeros((n_points,1))
        
        for index in range(n_points):
            if x[index] < x1:
                #Solution before x1
                rho[index] = rho_l
                P[index] = P_l
                u[index] = u_l
            elif (x1 <= x[index] and x[index] <= x2):
                #Solution between x1 and x2
                u[index] = 2/(gamma+1) *(a_l + (x[index]-x0)/t)
                a = a_l - (gamma-1)*u[index]/2
                P[index]=pow(a/a_l, (2*gamma)/(gamma-1))*P_l
                rho[index] = pow(P[index]/P_l, 1/gamma) * rho_l
                
            elif (x2 <= x[index] and x[index] <= x3):
                #Solution between x2 and x3
                rho[index] = rho_2
                P[index] = P_2
                u[index] = u_2
            elif (x3 <= x[index] and x[index] <= x4):
                #Solution between x3 and x4
                rho[index] = rho_1
                P[index] = P_1
                u[index] = u_1
            elif x4 < x[index]:
                #Solution after x4
                rho[index] = rho_r
                P[index] = P_r
                u[index] = u_r
            
            e[index] = P[index]/(gamma - 1) + 0.5*rho[index]*u[index]**2;
            m[index] = rho[index]*u[index]
            
            
    return data()



#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 01:08:49 2019

@author: callejonm
"""
#%% CONDICIONES INICIALES Y DE CONTORNO

from __future__ import print_function
from fenics import *
from mshr import *
import ufl as ufl
import numpy as np
import matplotlib as plt
import shutil


#Borrar graficas anteriores
#shutil.rmtree('graficas/')

#Initial parameters
T = 1               # final time
num_steps = 100     # number of time steps
dt = T / num_steps  # time step size
mu = 0.001           # dynamic viscosity
gamma=1.4
L = 10             #Longitud del mallado: dimensiones Lx0.1L  ¡¡EN BOUNDARY Y EXPRESIONS NO ESTÁ INCLUIDO!!
nx = 10           #Numero de celdas en dirección x
ny = 2            #Numero de celdas en dirección y

#Mesh
mesh = RectangleMesh( Point(0, 0), Point(L,0.1*L), nx, ny, 'right')

#Space Function 
Qe = FiniteElement('CG',mesh.ufl_cell(), 1)
Ve = VectorElement("CG", mesh.ufl_cell(), 2)

Q = FunctionSpace(mesh, Qe)
V = FunctionSpace(mesh, Ve)

W = FunctionSpace(mesh, MixedElement([Qe, Ve, Qe]))

# Define boundaries
inflow   = 'near(x[0], 0)'
outflow  = 'near(x[0], 10)'                   #10 es L ; 1 es 0.1*L
walls    = 'near(x[1], 0) || near(x[1], 1)'

# Define initial conditions

class InitialCondition(UserExpression):
    def eval_cell(self, values, x, ufc_cell):
        values[0] = 1
        values[1] = 0
        values[2] = 0
        
        mu=0.001                                # G: Gradiente de presión
        G=0.01
        p=G*(L-x[0])
                  
        u= 0.5*G/mu*x[1]*(1 - x[1])
        values[3]=p/0.4+0.5*u**2
    def value_shape(self):
        return (4,)
    

# Define boundary conditions

inflow_profile = ('0.01/2/0.001*x[1]*(1 - x[1])', '0')           # u=G/2/mu*y*(h-y)
outflow_energy = ('.5*25*x[1]*x[1]*(1 - x[1])*(1 - x[1])')      # p=0 E=0.5*u²

#bcu_inflow =  DirichletBC(W.sub(1), Expression(inflow_profile, degree=2), inflow)
bcu_walls =   DirichletBC(W.sub(1), Constant((0,0)), walls)
#bcp_outflow = DirichletBC(W.sub(2), Expression(outflow_energy, degree=4), outflow)

bcr_inflow =  DirichletBC(W.sub(0), Constant((1)), inflow)
bcr_walls =   DirichletBC(W.sub(0), Constant((1)), walls)
bcr_outflow = DirichletBC(W.sub(0), Constant((1)), outflow)

bc = [bcu_walls, bcr_inflow, bcr_walls, bcr_outflow]



# Define functions for solutions at previous (_n) and current (_)time steps and assign initial conditions
w = TrialFunction(W)
(r_test, m_test, s_test)=TestFunctions(W)
w_ = Function(W)
w_n = Function(W)

# Define expressions used in variational forms
#U  = 0.5*(u_n + u)
n  = FacetNormal(mesh)
f  = Constant((0, 0))
k  = Constant(dt)
mu = Constant(mu)
gamma = Constant(gamma)

def splitV(w):     # Función para visualización y output de datos, no permite calculos posteriores con r,u o p
    (r, m , s)=w.split()
    u=m/r
    p=(gamma-1)*(s-dot(m,m)/2/r)
    return (r,u,p)

def tau(mu,m,r):
    i,j=ufl.indices(2)
    u=m/r
    tau_u_ij= mu*(grad(u)[i,j]+grad(u)[j,i])-2/3*mu*div(u)*Identity(len(u))[i,j]
    tau_u = as_tensor(tau_u_ij, (i,j))
    
    tau_m_ij=tau_u[i,j]*r+mu/r*((m[i]*grad(r)[j]+m[j]*grad(r)[i])-2/3*dot(m,grad(r))*Identity(len(u))[i,j])
    tau_m = as_tensor(tau_m_ij, (i,j))
    
    return tau_m
  



# Save mesh to file (for use in reaction_system.py)
#File('navier_stokes_sod/cylinder.xml.gz') << mesh

file_r = File("graficas/density.pvd")
file_m = File("graficas/momentum.pvd")
file_s = File("graficas/energy.pvd")

# Create progress bar
progress = Progress('Time-stepping')
LogLevel.PROGRESS

# Apply initial condition
u_init = InitialCondition()
w_n.interpolate(u_init)
w_.interpolate(u_init)

(r,m,s)    = split(w)
(r_n,m_n,s_n) = split(w_n)

#(r,u,p)=splitV(w)
(r_n,u_n,p_n)=splitV(w_n)


h = 0.01  # approx 1/100
kappa=h

t = 0.0
stepcounter = 0.0

file_r.write(w_.split()[0], t)
file_m.write(w_.split()[1], t)
file_s.write(w_.split()[2], t)

#%% BUCLE SIMULACIÓN
for step in range(num_steps):
    # Formulación UPC
   # F1 = dot( r_test, (r-r_n)/k)*dx + dot(r_test, div(m_n))*dx 
      
    #F2 =dot( m_test, (m-m_n)/k)*dx + dot(m_test, -m_n/r_n**2*dot(m_n, grad(r_n)) + (gamma-1)* dot(m_n,m_n)/2/r_n**2*grad(r_n) + m_n/r_n*div(m_n)
    #+1/r_n*nabla_grad(m_n)*m_n - (gamma-1)/r_n*grad(m_n)*m_n + (gamma-1)*grad(s_n))*dx +\
    #-inner(-tau(mu,m_n,r_n),grad(m_test))*dx + inner(m_n,dot(-tau(mu,m_n,r_n), n))*ds

    #F3 = dot( s_test, (s-s_n)/k -(s_n+p_n)/r_n**2*dot(m_n,grad(r_n)) +(gamma-1)*dot(m_n,m_n)/2/r_n**3*dot(m_n,grad(r_n))+\
    #         (s_n+p_n)/r_n*div(m_n) -(gamma-1)*inner(outer(m_n,m_n),grad(m_n)) + gamma*dot(m_n/r_n,grad(s_n)))*dx +\
    #     -dot(-tau(mu,m_n,r_n)*m_n/r_n,grad(s_test))*dx \
    
    
    F1= dot(r_test, (r-r_n)/k)*dx - dot  (grad(r_test), m_n)*dx      + dot(r_test, dot(m_n,n))*ds   
    
    F2= dot(m_test, (m-m_n)/k)*dx - inner(grad(m_test), outer(m_n,m_n)/r_n + p_n*Identity(2))*dx \
    +mu*inner(grad(m_n/r_n),grad(m_test))*dx +  dot(m_test, m_n/r_n*dot(m_n,n) +p_n*n)*ds
    
    F3 = dot(s_test, (s-s_n)/k)*dx -dot(grad(s_test), (s_n+p_n)*m_n/r_n)*dx +dot(s_test, (s_n+p_n)/r_n*dot(m_n,n))*ds
    
    
    F=F1+F2+F3
    
    a=lhs(F)
    L=rhs(F)
    solve(a==L, w_, bc) 
    
    # Update current time
    t += dt


    (r_,m_,s_)=w_.split()
    plot(r_, title='Density')
  #  plot(u_, title='Velocity')
  #  plot(p_, title='Pressure')
    stepcounter+= 1;
    
    # Save solution to file (XDMF/HDF5)
    file_r.write(w_.split()[0], t)
    file_m.write(w_.split()[1], t)
    file_s.write(w_.split()[2], t)
    
    # Save nodal values to file
#    timeseries_r.store(u_.vector(), t)    
#    timeseries_u.store(u_.vector(), t)
#    timeseries_p.store(p_.vector(), t)
    

    # Update previous solution
    r_n = project(r_, Q)
    m_n = project(m_, V)
    s_n = project(s_, Q)
    p_n=(gamma-1)*(s_n-dot(m_n,m_n)/2/r_n)

    # Update progress bar
    progress=t / T
    
    if stepcounter % 10 ==0:
        print(progress)
        #print(s_.vector().max())
        print('m max:', max(m_.vector()))
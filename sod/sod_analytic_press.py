#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 01:09:43 2019

@author: callejonm
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 16:58:13 2019

@author: callejonm
"""

import numpy as np
from scipy.optimize import fsolve

def sod_func(P):
    #defines function to be used in analytic_sod
    #Initial conditions
    rho_l = 1
    P_l = 1
    u_l = 0

    rho_r = 0.125
    P_r = 0.1
    u_r = 0

    gamma = 1.4

    mu = pow(( (gamma-1)/(gamma+1) ), 0.5)

    y = (P - P_r)*(pow( ( ((1 - mu**2)**2)/((rho_r*(P + mu*mu*P_r))) ), (0.5))) \
        - 2*(pow(gamma, 0.5)/(gamma - 1))*(1 - pow(P, (gamma - 1)/(2*gamma)))
    return y

    

def solve(t, L, n_points):
 ##   t=0.2
  #  L=1
   # n_points=100
    
    x0 = 0.5*L
    rho_l = 1
    P_l = 1
    u_l = 0
    
    rho_r = 0.125
    P_r = 0.1
    u_r = 0
    
    gamma = 1.4
    mu = pow(( (gamma-1)/(gamma+1) ), 0.5)
    
    #Velocidad del sonido
    c_l = pow( (gamma*P_l/rho_l),0.5)
    c_r = pow( (gamma*P_r/rho_r),0.5)
    
    #P_post  = fsolve(sod_func,1)
    P_post = 3.031301780506468e-01
    v_post = 2*(pow(gamma, 0.5)/(gamma - 1))*(1 - pow(P_post, (gamma - 1)/(2*gamma)))
    rho_post = rho_r*(( (P_post/P_r) + mu**2 )/(1 + mu*mu*(P_post/P_r)))
    v_shock = v_post*((rho_post/rho_r)/( (rho_post/rho_r) - 1))
    rho_middle = (rho_l)*pow((P_post/P_l),1/gamma)
    
        
    #Key Positions
    x1 = x0 - c_l*t
    x3 = x0 + v_post*t
    x4 = x0 + v_shock*t
    #determining x2
    c_2 = c_l - ((gamma - 1)/2)*v_post
    x2 = x0 + (v_post - c_2)*t
    
    #start setting values
    #n_points = 1000   
    #boundaries (can be set)
    x_min = 0
    x_max = L
    
    class data:
        
        x = np.linspace(x_min,x_max,n_points)
        rho = np.zeros(n_points)
        P = np.zeros(n_points)
        u = np.zeros(n_points)
        e = np.zeros(n_points)
        m = np.zeros(n_points)
    data.x0 = x0
    data.x1 = x1
    data.x2 = x2
    data.x3 = x3
    data.x4 = x4    
    data.x_max = x_max
    for index in range(n_points):
        if data.x[index] < x1:
            #Solution before x1
            data.rho[index] = rho_l
            data.P[index] = P_l
            data.u[index] = u_l
        elif (x1 <= data.x[index] and data.x[index] <= x2):
            #Solution between x1 and x2
            c = mu*mu*((x0 - data.x[index])/t) + (1 - mu*mu)*c_l 
            data.rho[index] = rho_l*pow((c/c_l),2/(gamma - 1))
            data.P[index] = P_l*pow((data.rho[index]/rho_l),gamma)
            data.u[index] = (1 - mu*mu)*( (-(x0-data.x[index])/t) + c_l)
        elif (x2 <= data.x[index] and data.x[index] <= x3):
            #Solution b/w x2 and x3
            data.rho[index] = rho_middle
            data.P[index] = P_post
            data.u[index] = v_post
        elif (x3 <= data.x[index] and data.x[index] <= x4):
            #Solution b/w x3 and x4
            data.rho[index] = rho_post
            data.P[index] = P_post
            data.u[index] = v_post
        elif x4 < data.x[index]:
            #Solution after x4
            data.rho[index] = rho_r
            data.P[index] = P_r
            data.u[index] = u_r
        data.m[index]=data.rho[index]*data.u[index]
        data.e[index] = data.P[index]/(gamma - 1) + 0.5* data.rho[index]* data.u[index]**2
    return data



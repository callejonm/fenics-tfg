#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:09:51 2019

@author: callejonm
"""

from __future__ import print_function
from fenics import *
from mshr import *
from petsc4py import PETSc
from scipy.sparse import csr_matrix

import ufl as ufl
import numpy as np
import matplotlib as plt
import sod_analytic_press as exact
import shutil
import matrix_dij
import timing
#Borrar graficas anteriores
#shutil.rmtree('graficas/')
#Initial parameters

global t, L

def imprimircabecera(OPSION, nx, ny, dt, num_steps):
    print("\n")
    line = "-"*40
    if OPSION ==0:
        print(line)
        sim = "Simulacion con viscosidad mu=" 
        print(sim)
        caso = "3"
    if OPSION ==1: 
        print(line)
        sim = "Simulacion con viscosidad artificial d_ij"
        print(sim)
        caso = "d"
        
    print("Numero de elementos", nx, "x", ny)
    print("Paso temporal", dt, "(numero de pasos", num_steps, ")")
    return sim, caso

def createfiles(caso, nx, num_steps):
    dir_grafs = "graf_mu"+ caso + "_" + str(nx) + "_" + str(num_steps) + "/"
    fin_pvd   =     "_mu"+ caso + "_" + str(nx) + "_.pvd"
    
    
    filename_r  =  dir_grafs + "dens" + fin_pvd
    filename_m  =  dir_grafs + "mome" + fin_pvd
    filename_s  =  dir_grafs + "ener" + fin_pvd
    
    file_r = File( filename_r) 
    file_m = File( filename_m)
    file_s = File( filename_s)
    
    return file_r, file_m, file_s, dir_grafs
    
#def sod(num_steps, nx, OPSION):
  
    #Parámetros iniciales
nx = 80            # Numero de celdas en dirección x
num_steps = 160     # numero de pasos temporales
OPSION = 1          #0 para viscosidad normal, 1 para viscosidad d_ij

T = 0.2              # Tiempo final de simulación
dt = T / num_steps   # Delta t
#mu = 0.001          # Viscosidad
gamman=1.4           # Gamma del aire
L = 1                # Longitud del mallado: dimensiones Lx0.1L
ny = 4               # Numero de celdas en dirección y

delta_x = L/nx
CFL  = dt/delta_x

inicio = timing.step()

sim, caso = imprimircabecera(OPSION, nx, ny, dt, num_steps)

#Mallado FEniCS
mesh = RectangleMesh( Point(0, 0), Point(L,0.5*L), nx, ny, 'right')
n_vertex = (nx+1)*(ny+1)


#Mallado con GMSH: use dolfin-convert // meshio-convert mesh.msh mesh.xml 
#mesh = Mesh("mesh.xml")

#Function Spaces: Sin estabilización, 
#se requieren elementos Galerkin Continuos de orden 2

Qe = FiniteElement('CG', mesh.ufl_cell(), 1)

if OPSION ==0: 
    Ve = VectorElement('CG', mesh.ufl_cell(), 2)
elif OPSION ==1: 
    Ve = VectorElement('CG', mesh.ufl_cell(), 1)

Q = FunctionSpace(mesh, Qe)
V = FunctionSpace(mesh, Ve)

W = FunctionSpace(mesh, MixedElement([Qe, Ve, Qe]))

# Se definen funciones la solución en el paso anterior (_n) y actual (_)
w = TrialFunction(W)
(r_test, m_test, s_test)=TestFunctions(W)
w_ = Function(W)
w_n = Function(W)

# Define expresiones usadas en la forma variacional
#U  = 0.5*(u_n + u)
n  = FacetNormal(mesh)
f  = Constant((0, 0))
k  = Constant(dt)
cell = Cell(mesh, 5)
h = sqrt(cell.volume())
mu = Constant(h*0.1)
gamma = Constant(gamman)

def p(r, m, s):
    gamma =Constant(1.4)
    p=(gamma-1)*(s-dot(m,m)/2/r)
    return p

# Función para visualizaciÃ³n y output de datos, 
# no permite calculos posteriores con r,u o p (hay que proyectarlas)
def splitV(w):     
    (r, m , s)=w.split()
    u=m/r
    p=(gamma-1)*(s-dot(m,m)/2/r)
    return (r,u,p)

#Condiciones iniciales
class InitialCondition(UserExpression):
    def eval_cell(self, values, x, ufc_cell):
        if x[0] <= L/2:
            values[0] = 1
            values[1] = 0.0
            values[2] = 0.0
            values[3] = 1.0/(1.4-1.0)  +0.5*0**2/0.125
        else:
            values[0] = 0.125
            values[1] = 0.0
            values[2] = 0.0
            values[3] = 0.1/(1.4-1.0)  +0.5*0**2/0.125
    def value_shape(self):
        return (4,)

#Condiciones de contorno
inflow   = 'near(x[0], 0)'
outflow  = 'near(x[0], 1)'
walls    = 'near(x[1], 0) || near(x[1], 0.5)'
 
bcu_inflow = DirichletBC(W, Constant((1, 0, 0, 2.5)), inflow)
bcu_outflow = DirichletBC(W, Constant((.1, 0, 0, .25)), outflow)

bcw = [bcu_inflow, bcu_outflow]

#Solución exacta
class ExactSolution(UserExpression):
    def eval_cell(self, values, x, ufc_cell): 
        prec = nx
        solusio=exact.solve(t,L,prec)
        index = int((prec-1) * x[0]/L)

        values[0] = solusio.rho[index]
        values[1] = solusio.m[index]
        values[2] = 0
        values[3] = solusio.e[index]
    def value_shape(self):
        return (4,)
    




# Aplica condición inicial
u_init = InitialCondition()
w_n.interpolate(u_init)

(r,m,s)       = split(w)       #split() para Trial Function, 
(r_n,m_n,s_n) = w_n.split()    #.split() para Function

#(r,u,p)=splitV(w)
#(r_n,u_n,p_n)=splitV(w_n)


t = 0.0
stepcounter = 0.0


#Crea archivos a exportar

file_r, file_m, file_s, dir_grafs = createfiles(caso, nx, num_steps)
   
#Escribe valor inicial en el archivo
file_r.write(w_.split()[0], stepcounter)
file_m.write(w_.split()[1], stepcounter)
file_s.write(w_.split()[2], stepcounter)

error = np.zeros((num_steps,nx+1))

VECTOR_NODAL = w_.vector().vec().array

#%% Bucle temporal    
if OPSION ==0: 
    
    #Formulación variacional del problema
    F1= dot(r_test,  (r-r_n)/k)*dx - dot(grad(r_test), m_n)*dx  \
      + dot(r_test, dot(m_n,n))*ds   
        
    F2= dot(m_test, (m-m_n)/k)*dx -                           \
    inner(grad(m_test), outer(m_n,m_n)/r_n +                   \
                 (p(r_n,m_n,s_n))*Identity(2))*dx              \
    +  dot(m_test, m_n/r_n*dot(m_n,n) +(p(r_n,m_n,s_n))*n)*ds  \
    + mu*inner(grad(m_n/r_n),grad(m_test)      )*dx            \
    - mu*inner(m_test ,dot( grad( m_n/r_n) , n))*ds 
    
    F3 = dot(s_test, (s-s_n)/k)*dx \
    - dot(grad(s_test), (s_n+p(r_n,m_n,s_n))       * m_n/r_n    )*dx \
    + dot(     s_test,  (s_n+p(r_n,m_n,s_n))/  r_n * dot(m_n,n) )*ds
    
    F=F1+F2+F3
    
    a=lhs(F)
    Le=rhs(F)
    
    # Ensamblaje de matriz A (izquierda del sistema lineal)
    A_uv=assemble(a)  

    for step in range(num_steps):
        
        #Ensamblaje de matriz b, distinta para cada paso de tiempo
        b_v = assemble(Le)
        
        #Se aplican condiciones de contorno
        [bc_i.apply(A_uv,  b_v) for bc_i in bcw]
        
        #Resolución matricial
        solve(A_uv, w_.vector(), b_v)        
        
        #Actualización de paso de tiempo
        t += dt
        stepcounter+= 1;
        #Posibilidad de hacer plots dentro de las funciones integradas 
        #en FEnics -> Se elige Paraview
        #  plot(r_, title='Density')
        #  plot(u_, title='Velocity')
        #  plot(p_, title='Pressure')
        (r_,m_,s_) = w_.split()
        
        # Exporta solución a .pvd
        file_r.write(w_.split()[0], stepcounter)    
        file_m.write(w_.split()[1], stepcounter)
        file_s.write(w_.split()[2], stepcounter)
        
        # Actualiza solución anterior
        (r_,m_,s_) = w_.split(deepcopy = True)
        w_n.assign(w_)
        r_n.assign(r_)
        m_n.assign(m_)
        s_n.assign(s_)

        # Update progress bar
        progress=t / T*100
        
        # Comparación con solución exacta: en un array
        # m_solver = m_.vector().vec().getArray()
        vertex_values=m_n.sub(0).compute_vertex_values()
        
        #Obtención de valores en la línea media (y= 0.25; nx puntos en x)
        linea_media1= int( (ny-1)/2 * (n_vertex / (ny+1)) -1)                  
        linea_media2= int( (ny+1)/2 * (n_vertex / (ny+1)) -1)
        m_solver = vertex_values[linea_media1:linea_media2]    

        w_exacta = exact.solve(t,L,nx+1)
        m_exacta = w_exacta.m
        m_exacta = np.transpose(m_exacta)
        
        error [step]= m_solver - m_exacta
        
        #Output: error máximo en momento
        if progress % 1 <= 100/num_steps:
            line = "-"*40
            print("PROGRESO:", progress, "%")
            #print(s_.vector().max())
            print('Error max:', error[step].max(), 'en el elemento', np.argmax(error[step]))
            print(line)
            timing.log('')
            
elif OPSION ==1:
    
    #Formulación variacional del problema
    F1= dot(r_test,  (r-r_n)/k)*dx - dot(grad(r_test), m_n)*dx  \
      + dot(r_test, dot(m_n,n))*ds   
        
    F2= dot(m_test, (m-m_n)/k)*dx \
    - inner(grad(m_test), outer(m_n,m_n)/r_n)*dx \
    - inner(grad(m_test), (p(r_n,m_n,s_n))*Identity(2))*dx \
    +  dot(      m_test,  m_n/r_n*dot(m_n,n) +p(r_n,m_n,s_n)*n)*ds \
    #Término viscoso
    #+    mu*inner(grad(m_n/r_n),nabla_grad(m_test))*dx \
    #-    mu*inner(m_test, dot( grad( m_n/r_n) , n))*ds 
    
    F3 = dot(s_test, (s-s_n)/k)*dx \
    - dot(grad(s_test), (s_n+p(r_n,m_n,s_n))       * m_n/r_n    )*dx \
    + dot(     s_test,  (s_n+p(r_n,m_n,s_n))/  r_n * dot(m_n,n) )*ds
    
    F = F1+F2+F3
    
    a=lhs(F)
    Le=rhs(F)
    
    #Precalculo para matriz d
    MatC1, MatC2 = matrix_dij.MatC(W)
    
    #Matriz de grados de libertad de todos los vertices
    DOF   = np.array([W.sub(0).dofmap().entity_dofs(mesh,0),
                      W.sub(1).sub(0).dofmap().entity_dofs(mesh,0),
                      W.sub(1).sub(1).dofmap().entity_dofs(mesh,0),       
                      W.sub(2).dofmap().entity_dofs(mesh,0)])         
    idx = np.ix_(DOF[0], DOF[0])
    #Matriz de grados de libertad de todos las aristas
    #DOF_1 = np.array([W.sub(1).sub(0).dofmap().entity_dofs(mesh,1),
    #                  W.sub(1).sub(1).dofmap().entity_dofs(mesh,1)  ]) 
    
    #Ensamblaje de matriz de masa
    A_uv=assemble(a)  
    row,colum,val = as_backend_type(A_uv).mat().getValuesCSR()                               
    S = csr_matrix((val,colum,row))
    S.eliminate_zeros()
    
    for step in range(num_steps):

        #Ensamblaje del término de la derecha
        b_v = assemble(Le)
        
        # Al vector b_v se le aÃ±aden termino de  viscosidad artificial d_ij*u_n)
        dij, dmax = matrix_dij.petsc(mesh, W, MatC1, MatC2, DOF, S, w_n, t) #obtiene d_ij

        b_d =  dij * w_n.vector().vec()
        b_v.add_local(b_d)

        print("d maxima = ", dmax)
        
        #Resolución matricial
        solve(A_uv, w_.vector(), b_v)    
        
        # Update current time
        t += dt
    
        
        #Posibilidad de hacer plots dentro de las funciones integradas de FEnics -> Se elige Paraview
      #  plot(r_, title='Density')
      #  plot(u_, title='Velocity')
      #  plot(p_, title='Pressure')
        stepcounter+= 1;
        
        # Save solution to file (pvd)
        file_r.write(w_.split()[0], stepcounter)    
        file_m.write(w_.split()[1], stepcounter)
        file_s.write(w_.split()[2], stepcounter)
        
        # Update previous solution
        (r_,m_,s_) = w_.split(deepcopy = True)

        w_n.assign(w_)
        r_n.assign(r_)
        m_n.assign(m_)
        s_n.assign(s_)
    
        # Update progress bar
        progress=t / T*100
        
        # Comparate with exact solution: in an array
        #m_solver = m_.vector().vec().getArray()
        vertex_values=m_n.sub(0).compute_vertex_values()
        
        linea_media1= int( (ny-1)/2 * (n_vertex / (ny+1)) -1)                   #FUNCIONA CON ny=impar
        linea_media2= int( (ny+1)/2 * (n_vertex / (ny+1)) -1)
        m_solver = vertex_values[linea_media1:linea_media2]    # PUNTO INTERMEDIO (y= 0.25; nx puntos en x)
    
        w_exacta = exact.solve(t,L,nx+1)
        m_exacta = w_exacta.m
        m_exacta = np.transpose(m_exacta)
        
        error [step]= m_solver - m_exacta
        
        #Output: error mÃ¡ximo en momento
        if progress % 1 <= 100/num_steps:
            line = "-"*40
            print("PROGRESO:", progress, "%")
            #print(s_.vector().max())
            print('Error max:', error[step].max(), 'en el elemento', np.argmax(error[step]))
            timing.log('')
            print(line)
            
    class Ocupacion(UserExpression):
        def eval_cell(self, values, x, ufc_cell): 
            i = ufc_cell.index
            for aux in range(len(mesh.coordinates())):
                if mesh.coordinates()[aux][0] ==x[0] and mesh.coordinates()[aux][1] ==x[1]:
                    vert = aux


            oleee = DOF[0,vert]
            col, val = dij.getRow(oleee)
            
            values[0]= val.max()

            
        def value_shape(self):
            return (4,)
        
    okupa = Ocupacion()
    
    w_n.interpolate(okupa)
    file_nueva = File(dir_grafs + 'Ocupacion.pvd')
    file_nueva.write(w_n.split()[0])
#%% Comparate with exact solution: in a FEniCS function and pvd file.
timing.log('CÃ¡lculo de soluciÃ³n exacta')
         
file_exr = File( dir_grafs + "dens_" + str(nx) + "_ex.pvd")
file_exm = File( dir_grafs + "mome_" + str(nx) + "_ex.pvd")
file_exs = File( dir_grafs + "ener_" + str(nx) + "_ex.pvd")

w_ex  = Function(W)

exacta= ExactSolution()
w_ex.interpolate(exacta)

file_exr.write(w_ex.split()[0], stepcounter)    
file_exm.write(w_ex.split()[1], stepcounter)
file_exs.write(w_ex.split()[2], stepcounter)

#% Calculo de la norma L2 del error, mediante errornorm 
r_  = w_.split()[0]
r_ex = w_ex.split()[0]

L = np.zeros((2,4))

L[0, 0] = norm(w_.vector()-w_ex.vector(), 'l1')
L[0, 1] = norm(w_.vector()-w_ex.vector(), 'l2')
L[0, 2] = norm(w_.vector()-w_ex.vector(), 'linf')
L[0, 3] = errornorm(w_,w_ex, 'l2')
  
L[1, 0] = norm(r_.vector()-r_ex.vector(), 'l1')
L[1, 1] = norm(r_.vector()-r_ex.vector(), 'l2')
L[1, 2] = norm(r_.vector()-r_ex.vector(), 'linf')
L[1, 3] = errornorm(r_,r_ex, 'l2')


file_error = open("normas_error.txt","a")
intro = sim + ", mallado " + str(nx)+"x"+str(ny)\
 + " CFL = " + str(CFL) +": " + "\n"
file_error.write(intro)
file_error.write("Norma l1 error en w: "+     str(L[0,0]) + "\n")
file_error.write("Norma l2 error en w: " +    str(L[0,1])+ "\n")
file_error.write("Norma linf error en w: "+   str(L[0,2])+ "\n")
file_error.write("Norma l2 errornorm en w: "+ str(L[0,3])+ "\n \n")
    
file_error.write("Norma l1 error en r: "+     str(L[0,0])+ "\n")
file_error.write("Norma l2 error en r: "   +  str(L[0,1])+ "\n")
file_error.write("Norma linf error en r: "+   str(L[0,2])+ "\n")
file_error.write("Norma l2 errornorm en r: "+ str(L[1,3])+ "\n \n")



file_error.close()
#AMOAVE = A_uv.array()
#CONECT = mesh.cells()

# Obtención de tiempo final de la computación
tiempo = timing.steplog(inicio, "Final de simulaciÃ³n")

file_temp = open("tcomp.txt","a")
tcomp = "Tiempo de computacion para " + sim + ", mallado " + str(nx)+"x"+str(ny)\
 + " CFL = " + str(CFL) +" :    " + str(tiempo) + "segundos  " + " \n"
file_temp.write(tcomp) 
file_temp.close()


#for OPSION in range(2):
#for aux in range(7):
#OPSION = 0
#aux=2
#nx = 20 * pow(2, aux)
#CFL = 0.01
#T = 0.2
#num_steps = int(T*nx/CFL)
#print(nx)
#print(num_steps)
#sod(num_steps, nx, OPSION)
#        


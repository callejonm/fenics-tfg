#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 16:55:26 2019

@author: callejonm
"""
from fenics import *
from mshr import *
import numpy as np
from petsc4py import * 
from scipy.sparse import csr_matrix

def assemble_csr(a):                                                           
    A = assemble(a)  
    row,col,val = as_backend_type(A).mat().getValuesCSR()                               
    return csr_matrix((val,col,row))  

def MatC(W):
    
    #Space Function 
    (r_test, m_test, s_test) = TestFunctions(W)
    w_aux                    = TrialFunction(W)
    (r_aux, m_aux, s_aux)    = split(w_aux)
    
    #Construimos la matriz c_ij    
    
    F1a= dot(r_test.dx(0), r_aux)*dx
    F2a= dot(m_test.dx(0), m_aux)*dx
    F3a = dot(s_test.dx(0), s_aux)*dx
    Fa=F1a+F2a+F3a
    
    F1b = dot(r_test.dx(1), r_aux)*dx
    F2b = dot(m_test.dx(1), m_aux)*dx
    F3b = dot(s_test.dx(1), s_aux)*dx
    Fb=F1b+F2b+F3b
    
    ##nos vamos a quedar con los terminos de la densidad, pero se incluyen los otros
    ##para trabajar en el mismo espacio de soluciones (mismo orden de GDL)
    
    izdaa = lhs(Fa)
    izdab = lhs(Fb)
    
    MatC1 = assemble_csr(izdaa)
    MatC2 = assemble_csr(izdab)
    
    return MatC1, MatC2



def main(mesh, W, MatC1, MatC2, DOF, S, w_n):
    

    gamman = 1.4
    
    #%% Creación de varios mapeados y relaciones del mallado que van a ser útiles:

    tdim = mesh.topology().dim()
    
    mesh.init(tdim-1, 0)   # Init connectivity from facets to vertices
    mesh.init(0, 1 )       # Init connectivity from vertex(0) to edges(1)
    
# Crea un mapeado entre los vértices y las aristas del mallado
# Custom dictionary that always looks up sorted key
#print(v_2_f[(3, 4)])
# 
#    class MyDict(dict):
#        def get(self, key):
#            return dict.get(self, sorted(key))
#    
#    v_2_f = MyDict((tuple(facet.entities(0)), facet.index())
#                 for facet in facets(mesh))
#    
#    
# Crea una list que asigna a cada celda, sus celdas vecinas   
#    cell_neighbors = {cell.index(): sum((list(filter(lambda ci: ci != cell.index(),
#                                                facet.entities(tdim)))
#                                        for facet in facets(cell)), [])
#                    for cell in cells(mesh)}
    
    # Crea una matriz vecino cuyos elementos son los nodos vecinos de [i]
    def vecino(mesh, indice):
        v = Vertex(mesh, indice)
        vecino = [Edge(mesh, i).entities(0) for i in v.entities(1)]
        vecino = np.array(vecino).flatten()
    
        # Remove own index from vecino
        vecino = vecino[np.where(vecino != indice)[0]]
        return vecino
        #print("Vertex %d vecino: " %idx, vecino)
        
        
        
    def dot_product(a,b):                #cutre, lo defino para orden 2
        ans = a[0]*b[0]+a[1]*b[1]
        return ans
    
    def compute_lambda(ur,ul, cr, cl,nij):
        wn      = 0                                    #dot_product(vel,nij)
        landa   = max(abs(ur-wn)+cr, abs(ul-wn)+cl) 
        return landa
    
    def Dlow_elem(MatC1, MatC2, u, i, j, fila, col):
        #Matriz c
        cij = np.zeros(2)
        cji = np.zeros(2)
        
        cij[0]=MatC1[fila,col]          
        cij[1]=MatC2[fila,col]
        
        cji[0] = MatC1[col,fila]
        cji[1] = MatC2[col,fila]
        
    #                if np.logical_or(not cij.any() , not cji.any()):
    #                   print("EL ELEMENTO", fila, col, "ES CERO")
    #                  break
            
        normij = pow((cij[0]**2+cij[1]**2), 0.5)
        normji = pow((cji[0]**2+cji[1]**2), 0.5)
        
        
        nij = cij/normij
        nji = cji/normji
        
           # Euler 
        vel_eul_l = dot_product(u[i  ,1:3]/u[i  ,0] , nij) 
        vel_eul_r = dot_product(u[j  ,1:3]/u[j  ,0] , nij) 
        pre_l = max((gamman-1.0) * (u[i  ,3]-0.5*u[i  ,0]*vel_eul_l**2),1e-15)
        pre_r = max((gamman-1.0) * (u[j  ,3]-0.5*u[j  ,0]*vel_eul_r**2),1e-15)
        cl = pow((gamman*pre_l/u[i,0]),0.5)
        cr = pow((gamman*pre_r/u[j,0]),0.5)
        
        lambda1 = compute_lambda(vel_eul_r ,vel_eul_l, cr, cl, nij)
           
        vel_eul_l = dot_product(u[i  ,1:3]/u[i  ,0] , nji) 
        vel_eul_r = dot_product(u[j  ,1:3]/u[j  ,0] , nji) 
        pre_l = max((gamman-1.0) * (u[i  ,3]-0.5*u[i  ,0]*vel_eul_l**2),1e-15)
        pre_r = max((gamman-1.0) * (u[j  ,3]-0.5*u[j  ,0]*vel_eul_r**2),1e-15)
        cl = pow((gamman*pre_l/u[i,0]), 0.5)
        cr = pow((gamman*pre_r/u[j,0]), 0.5)
        
        lambda2 = compute_lambda(vel_eul_l,vel_eul_r, cl, cr, nji)
           
        Dlow_ij=max(lambda1*normij, lambda2*normji)
           
        if(Dlow_ij<0.0):
           print('No es Dij >0')
           
        return Dlow_ij
    
    
    #%%Cálculo de d_ij
        
    num_dofs = S.shape[0]
    num_vert = mesh.num_vertices()
    
    matCoor = mesh.coordinates()
    
    #Vector u con las coordenadas en cada vértice
    u = np.zeros((num_vert, 4))
    
    #u[:,0]  = w_n.sub(0).compute_vertex_values()          LENTO
    #u[:,1]  = w_n.sub(1).sub(0).compute_vertex_values() #[0:num_vert]
    #u[:,2]  = w_n.sub(1).sub(1).compute_vertex_values() #[num_vert:2*num_vert]
    #u[:,3]  = w_n.sub(2).compute_vertex_values()
    
    w_n_array =  w_n.vector().vec().getArray()     #RAPIDO
    for comp in range(4):
        u[:,comp] = w_n_array[DOF[comp]]
    
    d_ij = csr_matrix((num_dofs,num_dofs))
    Dlow = np.zeros((S.getnnz()))
    
    k=0


    for i in range(num_vert):
        fila = DOF[k][i]
 #       vecino_array = vecino(mesh, i)  # Usado en comprobación, costoso
 #       (waste, columnas) = S.getrow(fila).nonzero()
        suma=0
        for ind in range(S.indptr[fila], S.indptr[fila+1]):
            
            col = S.indices[ind]

            j    = np.where(DOF[k]==col)[0].item()
            if fila != col:
                Dlow_ij    = Dlow_elem(MatC1, MatC2, u, i, j, fila, col)
                Dlow[ind]  = Dlow_ij
                
                suma      = suma+Dlow[ind]
            else:
                indice  = ind
                
        Dlow[indice]  = -suma
            
##########################################################################
#  CON BUCLE EN k PARA CALCULAR CUATRO VECES Dlow, ES MUY               ## 
#  POCO EFICIENTE: CALCULA X4 VECES LO MISMO. MÁS EFICICIENTE           ##
#  HALLAR EL INDICE ind CORRESPONDIENTE A MOMENTO Y ENERGÍA             ##
#  (k=1,2,3) E IGUALARLO CON EL INDICE ind0 CORRESPONDIENTE A           ##
#  DENSIDAD.                                                            ##
#                                                                       ## 
#  EL MÉTODO INTENTADO DA ERROR EN LOS VALORES CONCRETOS, p. ej.        ##
#  ERROR = [8, 9, 13, 14, 18, 19, 29, 30, 34,                           ##
#             35, 39, 40, 43, 45, 46, 48, 49, 51]                       ##  
#  SE DEBE A QUE; PARA CIERTOS VALORES; j != j0 (EL VÉRTICE DEL GDL k   ## 
#  NO ES EL MISMO QUE EL GDL 0). SOLVENTADO CON if j0==j                ##
##########################################################################
    for k in range(1,4):
        for i in range(num_vert):
            fila0     = DOF[0][i]
            fila      = DOF[k][i]
            
            aux0 = range(S.indptr[fila0], S.indptr[fila0+1])
            aux  = range(S.indptr[fila] , S.indptr[fila+1])
            for ind0, ind in zip(aux0, aux):
                
                col0 = S.indices[ind0]
                col = S.indices[ind]
                
                j0    = np.where(DOF[0]==col0)[0].item()
                j     = np.where(DOF[k]==col)[0].item()
                if j0 == j:
                    Dlow[ind] = Dlow[ind0]
 
                else:
                    col = DOF[k][j0]
                    indice = np.where(S.indices[aux]==col) + S.indptr[fila]
                    Dlow[indice]=Dlow[ind0]

    return Dlow

def petsc(mesh, W, MatC1, MatC2, DOF, S, w_n, t):
    
    Dlow = main(mesh, W, MatC1, MatC2, DOF, S, w_n)
    
    dij = PETSc.Mat().createAIJ(size=S.shape, csr=(S.indptr, S.indices, Dlow))
    
    print("Viscosidad artificial calculada para tiempo t =", t)
        
    d_max = np.amax(Dlow)
    
    return dij, d_max


           